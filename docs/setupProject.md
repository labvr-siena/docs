# Setup del progetto

Una volta creato il progetto su Bitbucket, dobbiamo fare le modifiche necessarie 
per configurarlo per l’utilizzo con Unity.
Questa fase è composta da più parti, alcune utili ad un buon utilizzo di Git altre per un 
migliore utilizzo di Unity. Al momento consideriamo l’utilizzo di Unity, versione 2019.4 
(LTS). Segue la descrizione puntuale dei passi da fare, suddivisa in macro categorie.

## Download repository e modifiche preliminari

1. Scaricare in locale il progetto appena creato (tramite un client git, ad es. SourceTree).
2. Modificare (o sostituire) il file **.gitignore** con quello dei progetti Unity (si trova in cloud, nella 
   cartella configurazioni).
3. Aggiungere il file **.gitattribute** per abilitare il LFS di Git (si trova in cloud, nella cartella 
   configurazioni).
4. Aggiungere il file **users.txt** per la creazione dell’header nei file script di unity (si trova in cloud, nella 
   cartella configurazioni).
5. Fare il primo **push** delle modifiche al progetto, nel ramo **master**:
    * Fare il **commit** del file gitignore.
    * Fare il **commit** del file gitattribute.
    * Fare il **commit** del file users.txt (verificare che il proprio computer sia censito all’interno del file, con il 
      nostro nickname e la nostra e-mail).
      
## Creazione del progetto Unity e modifiche in editor

1. **Svuotare** il cestino e **cancellare** la cartella del progetto.
2. Avviare **Unity HUB**:
    * Selezionare **progetti**.
    * Premere il pulsante **nuovo**.
    * Selezionare il template **3D**.
    * Nelle configurazioni dare il nome al progetto, uguale alla cartella cancellata al passo 1.
    * Premere il pulsante **crea**.
3. Una volta aperto il progetto unity, **ripristinare** dal cestino la cartella cancellata.
4. Dentro **Unity** aprire il package manager:
    * Selezionare il menu **window**.
    * Premere su **Package Manager**.
5. Nella finsetra del Package manager:
    * Selezionare nel menù a tendina la voce **In Project**, per filtrare i risultati.
    * **Rimuovere** il package Rider Editor.
    * **Rimuovere** il package Test Framework.
    * **Aggiornare** (se necessario) il package TextMeshPro ed importare gli “Essentials”.
        * Premere il menu **window**.
        * Selezionare **TextMeshPro**.
        * Premere la voce Import **TMP Essential Resources**.
        * Confermare premendo il pulsante **import**.
    * **Rimuovere** il package Timeline.
    * **Rimuovere** il package Unity Collaborate.
    * **Rimuovere** il package Visual Studio Code Editor.
6. Fare un **push** delle modifiche di progetto, nel ramo **master**:
    * Fare il **commit** dei file relativi ai packages (nella cartella **Packages**):
        * manifest.json.
        * packages-lock.json.
    * Fare il **commit** dei file relativi alla configurazione del progetto (nella cartella **ProjectSettings**)e verificare che siano aggiunti nel **LFS** (tranne il file ProjectVersion):
        * All’interno della cartella sono presenti svariati file, sono tutti da aggiungere. In generale sono file **.asset**.
        * L’unica file diverso, al momento, è il file ProjectVersion.txt, anch’esso da aggiungere.

## Import delle librerie LabVR

Al momento questo passaggio consiste in una copia manuale delle cartelle delle librerie (di solito prese dal progetto 
“più avanti”, dati gli sviluppi costanti che stiamo effettuando). Nel prossimo futuro, avremo il progetto in un 
repository, dal quale effettuare la copia.

1. Importate il package **DOTween**, dall’asset store:
    * **Aprire** l’asset store (Ctrl + 9).
    * **Cercare** DOTween.
    * Selezionare la versione **free**.
    * Premere il pulsante **download**.
    * Al termine premere sul pulsante **importa**.
    * Nella finestra che appare **confermare** l’importazione di tutto il package.
    * Nella finestra di DOTween, dare **conferma** all’apertura del pannello di configurazione del package.
    * Premere il pulsante per **eseguire il setup** del package.
    * Premere il pulsante **applica**, per applicare le modifiche.
2. Importare la libreria **RestSharp** (al momento versione 106.11.4).
3. Copiare il file csc (si trova in cloud, nella cartella configurazioni).
4. Importate il package **Oculus**, dall’asset store:
    * **Aprire** l’asset store (Ctrl + 9).
    * **Cercare** Oculus.
    * Premere il pulsante **download**.
    * Al termine premere sul pulsante **importa**.
    * Nella finestra che appare **confermare** l’importazione di tutto il package.
    * Attivare il plugin nella nuova finestra, premendo **si**.
    * Quando chiede se riavviare, premere il pulsante **non ora**.
    * Nella finestra successiva premere nuovamente su **si**, per aggiornare un sub package.
    * Chiudere e **riavviare** Unity.
5. Copiare la cartella **LabVR**, delle nostre librerie.
6. Fare un **push** delle modifiche apportate al progetto:
    * Fare il **commit** del package di DOTween:
        * tutti i file dentro la cartella Demigiant (dentro Plugins).
        * il file Demigiant.meta (il file meta della cartella).
        * i file di configurazione DOTweenSettings.asset ed il suo file meta (dentro la cartella Resources).
    * Fare il **commit** del plugin RestSharp:
        * tutti i file dentro la cartella RestSharp.
        * il file meta della cartella RestSharp.
    * Fare il **commit** del file csc e relativo meta.
    * Fare il **commit** delle configurazioni del plugin Oculus:
        * Il file OVRPlatformToolSettings.asset ed il suo file meta.
    * Fare il **commit** del package LabVR:
        * tutti i file presenti dentro la cartella LabVR.
        * il file meta relativo della cartella LabVR.

## Configurazione per la build android

In questa parte vediamo i passaggi necessari per poter configurare correttamente Unity, in modo da esportare 
il progetto per Oculus Quest.

1. Cambiare la piattaforma per la quale si genera il progetto:
    * Tramite il menu File selezionare **Build Settings**....
    * Nella finestra aperta, selezionare **Android**.
    * Premere il pulsante **Cambia piattaforma**.
    * Nella voce “Texture Compression” selezionare **ASTC**.
2. Cambiare le configurazioni per il progetto:
    * Aprire nuovamente i project setting, tramite il menu **Edit** e selezionare **Project Settings**….
    * Selezionare **Player**:
        * Cambiare la voce **Company Name** con Simula.
        * Nella voce **Product Name** inserire il nome del progetto.
        * Alla voce **Version** inserire il valore 1.0.0.
        * Alla voce **API Compatibily Level** selezionare .NET 4.x.
    * Nella sezione **Identification**:
        * Nella voce **Package Name** inserire com.Simula.nomeProgetto.
        * Alla voce **Version** inserire lo stesso valore inserito al punto precedente.
        * Nella voce **Boundle Version Code** lasciare il numero 1.
        * Alla voce **Minimum API Level** selezionare Android 6.0 ‘Marshmallow’ (API Level 23).
3. Abilitare il plugin nativo di Unity per l’**XR**:
    * Aprire i project setting, tramite il menu **Edit** e selezionare **Project Settings**….
    * Nella finestra, selezionare la voce **XR Plugin-management**:
        * Premere il pulsante **importa**.
        * Selezionare il **simbolo android**.
        * Mettere la spunta sulla voce **Oculus**.
        * Mettere la spunta sul **Low Overhead Mode**.
4. Cambiare le configurazioni di Rendering:
    * Aprire nuovamente i project setting, tramite il menu **Edit** e selezionare **Project Settings**….
    * Selezionare **Player**:
    * Selezionare la voce **Other Settings**:
        * Espandere la sezione **Rendering**.
        * Impostare la voce **Color Space** su “Linear”.
        * Togliere la spunta alla voce **Auto Graphic API**:
            * **Rimuovere** la voce “Vulkan” se presente.
            * Mettere come prima voce il valore **Open GLS 3.0**.
        * Mettere la spunta alla voce **Multithreaded Rendering**.
5. Cambiare le configurazioni di Qualità:
    * Aprire nuovamente i project setting, tramite il menu **Edit** e selezionare **Project Settings**….
    * Selezionare **Quality**:
        * Selezionare medium tra i **livelli di qualità** (quello di android).
        * Alla voce **Pixel Light Count** impostare come valore massimo 1.
        * Selezionare per **Texture Quality** il valore “Full Res”.
        * Alla voce **Anisotropic Textures** scegliere “Per Texture”.
        * Per la voce **Anti Aliasing** selezionare “4x”.
        * Rimuovere la spunta alla voce **Soft Particles**.
        * Selezionare la voce **Realtime Reflections Probes**.
        * Mettere la spunta alla voce **Billboards Face Camera**.
6. Fare un **push** delle modifiche apportate al progetto:
    * Fare il **commit** delle modifiche per il plugin XR di Unity:
        * manifest.json.
        * packages-lock.json.
        * tutti i file presenti dentro la cartella XR, compreso il meta della cartella.
    * Fare il **commit** delle modifiche di configurazione del progetto:
        * Selezionare tutti i file dentro la cartella Project settings (ricordandosi di verificare che siano inseriti come file LFS).































    











    






















    







