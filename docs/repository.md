# Creazione Repository

Il primo passo, per ogni progetto, è la creazione del repository Git. 
Al momento ci stiamo affidando al servizio di BitBucket (Atlassian). 
Ne seguono questi passi:

1. Fare il login su **Bitbucket**.
2. Selezionare il **Workspace** LabVR Siena.
3. Selezionare il **Project** nel quale inserire il nuovo repository (o crearne uno nuovo, se assente).
    * Per il naming di solito usiamo LabVR ed una descrizione breve (ad es. Pharma, RubesTriva, Vigili del Fuoco).
4. Creare il Repository del progetto (verificare che appartenga al workspace ed al progetto, di cui ai punti 2 e 3).
    * Per il naming di solito usiamo LabVR ed una descrizione (ad es. LabVR Pharma Pipette, LabVR VVF, 
      LabVR Rubes Triva ALIA).
    * Includere il file .gitignore.
    * Nelle impostazioni avanzate:
        * Inserire una descrizione.
        * Selezionare come linguaggio il C#.