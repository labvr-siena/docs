# Introduzione

In questo documento vogliamo riassumere i passi necessari e 
fondamentali per creare un progetto di realtà virtuale con gli 
strumenti da noi sviluppati. Una specie di guida “passo passo” 
per poter inziare un progetto “allo stato dell’arte”.


## Prerequisiti
Elenchiamo di seguito tutte le necessità per poter avviare un nuovo progetto VR:

* [**Bitbucket**](https://bitbucket.org/) - repository remoto Git.
* [**Unity**](https://unity.com/) - Real time engine.
* **Cloud storage** - GDrive al momento e poi One drive